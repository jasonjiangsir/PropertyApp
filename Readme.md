## HC掌上物业

HC掌上物业是完全免费开源手机版的小区物业管理系统，采用目前比较主流的uni-app和colorui技术，一次编码多端运行，
包括Android、ios 、H5和小程序。
目前包含的功能有:维修录单、维修受理、投诉录单、投诉受理、巡检打卡、采购管理、公告、通讯录、小区广告和小区文化等。
了解更多请加交流群：827669685

## HC官网

[http://www.homecommunity.cn](http://www.homecommunity.cn)

## 二次开发视频

[https://space.bilibili.com/403702784](https://space.bilibili.com/403702784)

## 运行截图

![image](/readme/img/login.png)
![image](/readme/img/index.png)
![image](/readme/img/repair.png)
![image](/readme/img/compaint.png)
![image](/readme/img/inspection.png)
![image](/readme/img/notice.png)
![image](/readme/img/tels.png)
![image](/readme/img/my.png)